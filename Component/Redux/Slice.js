import { createSlice } from "@reduxjs/toolkit"

const SliceAPI = createSlice({
    name:"Food Api",
    initialState:{
        APIStore:[],
        LoginUser:[],
        AddOrderList:[]
    },
    reducers:{
        getCallApi:(state,action)=>{
            state.APIStore = action.payload
        },
        getADD:(state,action)=>{
            state.LoginUser = action.payload
        },
        getAddOrder:(state,action)=>{
             state.AddOrderList.push(action.payload)
        },
        getCanselOrder:(state,action)=>{
            state.AddOrderList = state.AddOrderList.filter((item)=>item?.id !== action.payload)

        }
    }
})
export const {getCallApi,getADD,getAddOrder,getCanselOrder} = SliceAPI.actions
export default SliceAPI.reducer


