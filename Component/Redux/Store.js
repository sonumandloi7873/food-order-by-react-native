
import { configureStore } from '@reduxjs/toolkit'
import SliceAPI from './Slice'


const store = configureStore({
    reducer:{
        cartData:SliceAPI
    }
})

export default store 