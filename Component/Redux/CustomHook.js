import { useEffect } from "react"
import { apiData } from "./utility";
import { useDispatch } from "react-redux";
import { getCallApi } from "./Slice";

const useApiCall = () => {

    const dispatch = useDispatch()
    async function apiCall() {
        const api = await fetch(`${apiData}`);
        const responData = await api.json();
        const respoList = responData?.data?.cards
        dispatch(getCallApi(respoList))
        // console.log("responData",respoList)
        // console.warn("responData",responData?.data?.cards[3]?.card?.card?.gridElements)
    }

    useEffect(() => {
        apiCall()
    },[])


}

export default useApiCall