import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Entypo from 'react-native-vector-icons/Entypo'

const Wallet = () => {

    

    return (
        <View>
            <View style={styles.expoler}>
                <View style={styles.titlteRow}></View>
                <Text style={styles.expolerTitle}>WALLET</Text>
                <View style={styles.titlteRow}></View>
            </View>
            <View style={styles.cards}>
                <View style={styles.gift}>
                    <View style={styles.gift}>
                        <Text style={styles.WalletIcon}>
                            <Entypo name="wallet" size={25} color="red" />
                        </Text>
                        <Text style={[styles.WalletText,styles.WalletText2]}>Zomato Money</Text>
                    </View>
                        <Text style={styles.Walletrs}>Rs 0</Text>
                </View>
                <View style={styles.gift}>
                    <Text style={[styles.WalletText,{marginTop:5}]}>Gift Card Balance</Text>
                    <Text style={styles.Walletrs}>Rs 0</Text>

                </View>
                <View style={styles.line}></View>

                <View style={styles.gift} >
                    <Text style={styles.WalletText}>Zomato Credits</Text>
                    <Text style={styles.Walletrs}>Rs 0</Text>
                </View>
            </View>
        </View>
    )
}

export default Wallet

const styles = StyleSheet.create({
    expoler: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    expolerTitle: {
        marginHorizontal: 15,
        letterSpacing: 5,
        color: "black",
        fontSize: 15
    },
    titlteRow: {
        width: 130,
        height: 1,
        backgroundColor: "#999999"
    },
    cards:{
        width:"100%",
        // borderWidth:1,
        height:120,
        borderRadius:20,
        backgroundColor:"white",
        padding:10,
        paddingHorizontal:20,
        marginTop:20
    },
    gift:{
        display:"flex",
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row"
    },
    WalletIcon:{
        backgroundColor:'navy',
        padding:5,
        borderRadius:50,

    },
    WalletText:{
        fontSize:18,
        color:"black"
    },
    WalletText2:{
       marginHorizontal:10,
       fontSize:23,

    },
    line:{
        width:"100%",
        height:1,
        backgroundColor:"grey",
        marginBottom:5,
        marginTop:5
    },
    Walletrs:{
        fontSize:18,
        color:"black"
    }
})