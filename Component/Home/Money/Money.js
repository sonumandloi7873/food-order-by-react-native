import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Zmoney from './Zmoney'
import Wallet from './Wallet'
import Order from './Order'

const Money = () => {
  return (
    <ScrollView>
      <Zmoney/>
      <Wallet/>
      <Order/>
    </ScrollView>
  )
}

export default Money

const styles = StyleSheet.create({})