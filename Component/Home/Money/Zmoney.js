import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import LocationIcon from 'react-native-vector-icons/Ionicons';

const Zmoney = () => {
    return (
        <>
            <View style={styles.container}>
                <View style={styles.container_head}>
                    <View style={styles.container_head_location}>
                        <LocationIcon name="location-sharp" size={30} color="red" />
                        <Text style={styles.container_head_location_text}>Indore</Text>
                        <LocationIcon name="chevron-down-sharp" size={20} color="red" />
                    </View>
                    <View style={styles.container_head_admin}>
                        <Text style={styles.container_head_admin_text}>S</Text>
                    </View>
                </View>
            </View>
            <View style={styles.money}>
                <Text style={styles.moneyText}>MONEY</Text>
            </View>
        </>
    )
}

export default Zmoney

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "column",
        backgroundColor: 'transparent',
        padding: 20,
        zIndex: 100,
        height:200
    },
    container_head: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: 20

    },
    container_head_location: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    container_head_location_text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "red"
    },
    container_head_admin: {
        backgroundColor: "#ADD8E6",
        borderRadius: 20,
        // borderWidth:1,
        width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: 43,

    },
    container_head_admin_text: {
        color: "blue",
        fontSize: 30,

    },
    money: {
        backgroundColor: "navy",
        height: 180,
        position: "absolute",
        width: "100%",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        borderBottomRightRadius:25,
        borderBottomLeftRadius:25,
    },
    moneyText: {
        color: "white",
        marginBottom: 20,
        paddingBottom: 20,
        fontSize:40,
        fontWeight:"bold",
        letterSpacing:5,
    }
})