import { Button, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { imageURL } from '../../Redux/utility'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { getCanselOrder } from '../../Redux/Slice'

const Order = () => {

    const data = useSelector((state) => state?.cartData?.AddOrderList)
    const dispatch = useDispatch()

    console.log("order",data)

    return (
        <View>
            <View style={styles.expoler}>
                <View style={styles.titlteRow}></View>
                <Text style={styles.expolerTitle}>Order</Text>
                <View style={styles.titlteRow}></View>
            </View>
            <View>
                {
                    data?.map((item) => {
                        return (
                            <>
                                <View key={item?.id}  style={styles.container} >
                                    <View style={styles.cardImage}>
                                        <Image style={styles.Image}
                                            source={{ uri: `${imageURL}${item?.cloudinaryImageId}` }} />
                                    </View>
                                    <View style={styles.cardDeatil}>
                                        <View style={styles.cardName}>
                                            <Text style={styles.cardRest}>{item?.name.slice(0, 20)}...</Text>
                                            <View style={styles.cardstars}>
                                                <Text style={styles.cardstar}>{item?.avgRating} </Text>
                                                <AntDesign name="star" size={16} style={{ color: "white" }} />
                                            </View>
                                        </View>
                                        <View style={styles.cardArea}>
                                            <Text style={styles.cardareaName}>{item?.cuisines.slice(0, 3)}...</Text>
                                            <Text style={styles.cardCost}>{item?.costForTwo}</Text>
                                        </View>
                                        <View style={styles.cardTimeOrder}>
                                            <View style={styles.cardTimes}>
                                                <Text style={styles.cardTime}>{item?.sla?.slaString}</Text>
                                                <Text>*</Text>
                                                <Text style={styles.cardDistance}>{item?.sla?.lastMileTravelString}</Text>
                                            </View>
                                            <Button title='Cansel' onPress={()=>dispatch(getCanselOrder(item?.id))} />
                                        </View>
                                        <View style={{ width: "100%", height: 1, backgroundColor: "grey" }}></View>
                                        <View style={styles.cardOffer}>
                                            <Text style={styles.cardOffer1}>{item?.aggregatedDiscountInfoV3?.header}</Text>
                                            <Text style={styles.cardOffer2}>{item?.aggregatedDiscountInfoV3?.subHeader}</Text>
                                        </View>
                                    </View>
                                </View>
                            </>
                        )
                    })}
            </View>
        </View>

    )
}

export default Order

const styles = StyleSheet.create({
    expoler: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    expolerTitle: {
        marginHorizontal: 15,
        letterSpacing: 5,
        color: "black",
        fontSize: 15
    },
    titlteRow: {
        width: 130,
        height: 1,
        backgroundColor: "#999999"
    },
    cardTitle: {
        textAlign: "center",
        color: "grey",
        fontSize: 15,
        marginTop: 10
    },
    title: {
        textTransform: "uppercase",
    },
    container: {
        width: 410,
        height: 360,
        marginTop: 30,
        borderRadius: 10
    },
    cardImage: {
        width: 410,
        height: 220,
    },
    Image: {
        width: 410,
        height: 220,
        resizeMode: "cover",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    cardDeatil: {
        width: 410,
        height: 140,
        borderWidth: 1,
        padding: 10,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    cardName: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    cardRest: {
        fontSize: 25,
        color: "black",
        fontWeight: "500"
    },
    cardstars: {
        width: 60,
        height: 30,
        borderRadius: 10,
        backgroundColor: "green",
        paddingHorizontal: 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
    },
    cardstar: {
        fontSize: 18,
        fontWeight: "500",
        color: "white",
    },
    cardArea: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 3,
    },
    cardareaName: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
    },
    cardCost: {
        color: "grey",
        letterSpacing: 2,
        fontSize: 15,
        marginLeft: 10
    },
    cardTimes: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 5,
        marginBottom: 5,
    },
    cardTimeOrder:{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent:"space-between",
        marginBottom: 2,
    },
    cardTime: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
    },
    cardDistance: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
        marginLeft: 10
    },
    cardOffer: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 5,
    },
    cardOffer1: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
        color: "blue"
    },
    cardOffer2: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
        color: "blue",
        marginLeft: 10
    },
})





// {"analytics":
//  {"context": "seo-data-3921c871-62ca-40fa-962b-2ea69d4234d4"}, 
// "cta": {"link": "https://www.swiggy.com/restaurants/ubq-by-barbeque-nation-treasure-island-mall-tukoganj-indore-322540", 
// "text": "RESTAURANT_MENU", "type": "WEBLINK"}, 
// "info": 
    //  {"aggregatedDiscountInfoV3": 
                //    {"discountTag": "MIN", "header": "₹70 OFF", "subHeader": "ABOVE ₹149"},
                //  "areaName": "Tukoganj",
                //  "availability": {"nextCloseTime": "2024-04-18 23:00:00", "opened": true}, 
                // "avgRating": 4.1,
                //  "avgRatingString": 
                // "4.1", "badges": {}, "badgesV2": {"entityBadges": [Object]}, "cloudinaryImageId": "ousldxwz6z3cypd2pygf", "costForTwo": "₹300 for two", "cuisines": ["North Indian", "Barbecue", "Biryani", "Kebabs", "Mughlai", "Desserts"], "differentiatedUi": {"differentiatedUiMediaDetails": [Object], "displayType": "ADS_UI_DISPLAY_TYPE_ENUM_DEFAULT"}, "displayType": "RESTAURANT_DISPLAY_TYPE_DEFAULT", "id": "322540", "isOpen": true, "locality": "Treasure Island Mall", "name": "UBQ BY Barbeque Nation", "orderabilityCommunication": {"customIcon": [Object], "message": [Object], "subTitle": [Object], "title": [Object]}, "parentId": "10804", "restaurantOfferPresentationInfo": {}, "reviewsSummary": {}, "sla": {"deliveryTime": 45, "iconType": "ICON_TYPE_EMPTY", "lastMileTravel": 2.6, "lastMileTravelString": "2.6 km", "serviceability": "SERVICEABLE", "slaString": "40-45 mins"}, "totalRatingsString": "1K+", "type": "F"}, "widgetId": "collectionV5RestaurantListWidget_SimRestoRelevance_food_seo"}