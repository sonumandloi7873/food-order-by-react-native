import { FlatList, Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux'
import { imageURL } from '../../Redux/utility'
import { SafeAreaView } from 'react-native-safe-area-context'

const Product = () => {

    const data = useSelector((state) => state?.cartData?.APIStore)
    // card?.card?.imageGridCards?.info
    const {card={}} = data[0] || []
    const {card:item ={}} = card || []
    const {imageGridCards={}} = item || []
    const {info=[]} = imageGridCards || []
    // console.log("product",info)

    return (
        <View style={{width:"100%"}}>
            <View style={styles.expoler}>
                <View style={styles.titlteRow}></View>
                <Text style={styles.expolerTitle}>WHAT'S ON YOUR MIND?</Text>
                <View style={styles.titlteRow}></View>
            </View>
            <SafeAreaView style={{marginTop:20}}>

            <ScrollView horizontal={true}  >
                {
                    info?.map((item)=>{
                        return(
                            <View key={item?.id} style={{padding:10}} >
                                <Image 
                                width={130}
                                height={130}
                                borderRadius={25}
                                source={{
                                    uri:`${imageURL}${item?.imageId}`
                                }} />
                            </View>
                        )
                    })
                }
                {/* <FlatList 
                data={data}
                renderItem={({item})=><Image 
                width={130}
                height={130}
                borderRadius={5}
                source={{uri:`${imageURL}${item?.imageId}`}} />}
                /> */}
            </ScrollView>
            </SafeAreaView>

        </View>
    )
}

export default Product

const styles = StyleSheet.create({
    expoler: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    expolerTitle: {
        marginHorizontal: 15,
        letterSpacing: 5,
        color: "black",
        fontSize: 15
    },
    titlteRow: {
        width: 130,
        height: 1,
        backgroundColor: "#999999"

    },
})