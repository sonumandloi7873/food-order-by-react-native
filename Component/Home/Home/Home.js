import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import FreeDelivery from './FreeDelivery'
import Explore from './Explore'
import Product from './Product'
import Restaurants from './Restaurants'
import useApiCall from '../../Redux/CustomHook'

const Home = () => {
  useApiCall()

  return (
    <>
      <ScrollView>
        <FreeDelivery />
        <Explore/>
        <Product/>
        <Restaurants/>
      </ScrollView>
    </>
  )
}

export default Home

const styles = StyleSheet.create({})