import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { localRestaurants, offer, playandwin } from './Image'

const Explore = () => {
    return (
        <View>
            <View style={styles.expoler}>
                <View style={styles.titlteRow}></View>
                <Text style={styles.expolerTitle}>EXPLORE</Text>
                <View style={styles.titlteRow}></View>
            </View>

            <View style={styles.expolerBox}>
                <View style={styles.expolerBox1}>
                    <Image
                        style={styles.expolerImage}
                        source={{
                            uri: `${offer}`
                        }} />
                    <Text style={styles.imageTitle}>Offers</Text>
                </View>
                <View style={styles.expolerBox1}>
                    <Image style={[styles.expolerImage, styles.expolerImage2]}
                        source={{
                            uri: `${playandwin}`
                        }} />
                    <Text  style={styles.imageTitle}>Play & win</Text>
                </View>
                <View style={styles.expolerBox1}>
                    <Image style={[styles.expolerImage, styles.expolerImage3]}
                        source={{
                            uri: `${localRestaurants}`
                        }} />
                    <Text style={styles.imageTitle}>Restaurants</Text>
                </View>
            </View>
        </View>
    )
}

export default Explore

const styles = StyleSheet.create({
    expoler: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    expolerTitle: {
        marginHorizontal: 15,
        letterSpacing: 5,
        color: "black",
        fontSize: 15
    },
    titlteRow: {
        width: 130,
        height: 1,
        backgroundColor: "#999999"

    },
    expolerBox: {
        display: "flex",
        flexDirection: "row",
        gap: 20,
        padding: 5,
        marginTop: 20,

    },
    expolerBox1: {
        width: 120,
        height: 130,
        borderWidth: 1,
        borderColor:'grey',
        borderRadius: 10,
        backgroundColor: 'white'

    },
    expolerImage: {
        width: 118,
        height: 110,
        borderRadius: 12,

    },
    expolerImage2: {
        resizeMode: "contain",
    },
    expolerImage3: {
    },
    imageTitle:{
        position:"absolute",
        bottom:5,
        textAlign:"center",
        fontSize:16,
        color:"black",
        width:117,
        backgroundColor:'pink'
    }
})