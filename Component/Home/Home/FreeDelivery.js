import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import NavBar from '../../NavBar/NavBar'

const FreeDelivery = () => {
    return (
        <View>
            <View style={{ height: 350 }}>
                <NavBar />
                <Image
                    style={styles.image}
                    source={{
                        uri: "https://img.etimg.com/thumb/width-1200,height-900,imgsize-1963013,resizemode-75,msid-97275855/tech/technology/zomato-gold-returns-with-discounts-on-delivery-and-dining-out.jpg"
                    }} />
            </View>
        </View>
    )
}

export default FreeDelivery

const styles = StyleSheet.create({
    image: {
        width: 410,
        height: 350,
        position: "absolute",
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        resizeMode: "stretch"
    },

})