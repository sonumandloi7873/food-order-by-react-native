import { Button, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { imageURL } from '../../Redux/utility'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { getAddOrder } from '../../Redux/Slice'

const Restaurants = () => {

    const data = useSelector((state) => state?.cartData?.APIStore)
    const dispatch = useDispatch()

    const { card = {} } = data[4] || []
    const { card: item = {} } = card || []
    const { gridElements = {} } = item || []
    const { infoWithStyle = {} } = gridElements || []
    const { restaurants = [] } = infoWithStyle || []
    // console.log("card", card)
    // console.log("gridElements", item)
    // console.log("infoWithStyle", infoWithStyle)
    // console.log("restaurants", restaurants)

    const addCardorder = (data)=>dispatch(getAddOrder(data))

    return (
        <View>
            <View style={styles.expoler}>
                <View style={styles.titlteRow}></View>
                <Text style={styles.expolerTitle}>ALL RESTAURANTS</Text>
                <View style={styles.titlteRow}></View>
            </View>

            <View>
                <Text style={styles.cardTitle} >196 restaurants delivering to you</Text>
                <Text style={[styles.title, styles.cardTitle]}>featured</Text>
                {
                    restaurants?.map((item) => {
                        return (
                            <>
                                <View key={item?.info?.id} style={styles.container}>
                                    <View style={styles.cardImage}>
                                        <Image style={styles.Image}
                                            source={{ uri: `${imageURL}${item?.info?.cloudinaryImageId}` }} />
                                    </View>
                                    <View style={styles.cardDeatil}>
                                        <View style={styles.cardName}>
                                            <Text style={styles.cardRest}>{item?.info?.name.slice(0, 20)}...</Text>
                                            <View style={styles.cardstars}>
                                                <Text style={styles.cardstar}>{item?.info?.avgRating} </Text>
                                                <AntDesign name="star" size={16} style={{ color: "white" }} />
                                            </View>
                                        </View>
                                        <View style={styles.cardArea}>
                                            <Text style={styles.cardareaName}>{item?.info?.cuisines.slice(0, 3)}...</Text>
                                            <Text style={styles.cardCost}>{item?.info?.costForTwo}</Text>
                                        </View>
                                        <View style={styles.cardTimeOrder}>
                                            <View style={styles.cardTimes}>
                                                <Text style={styles.cardTime}>{item?.info?.sla?.slaString}</Text>
                                                <Text>*</Text>
                                                <Text style={styles.cardDistance}>{item?.info?.sla?.lastMileTravelString}</Text>
                                            </View>
                                            <Button title='Order' onPress={()=>addCardorder(item?.info)} />
                                        </View>
                                        <View style={{ width: "100%", height: 1, backgroundColor: "grey" }}></View>
                                        <View style={styles.cardOffer}>
                                            <Text style={styles.cardOffer1}>{item?.info?.aggregatedDiscountInfoV3?.header}</Text>
                                            <Text style={styles.cardOffer2}>{item?.info?.aggregatedDiscountInfoV3?.subHeader}</Text>

                                        </View>
                                    </View>
                                </View>
                            </>
                        )
                    })}
            </View>
        </View>

    )
}

export default Restaurants

const styles = StyleSheet.create({
    expoler: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    expolerTitle: {
        marginHorizontal: 15,
        letterSpacing: 5,
        color: "black",
        fontSize: 15
    },
    titlteRow: {
        width: 130,
        height: 1,
        backgroundColor: "#999999"
    },
    cardTitle: {
        textAlign: "center",
        color: "grey",
        fontSize: 15,
        marginTop: 10
    },
    title: {
        textTransform: "uppercase",
    },
    container: {
        width: 410,
        height: 360,
        marginTop: 30,
        borderRadius: 10
    },
    cardImage: {
        width: 410,
        height: 220,
    },
    Image: {
        width: 410,
        height: 220,
        resizeMode: "cover",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    cardDeatil: {
        width: 410,
        height: 140,
        borderWidth: 1,
        padding: 10,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    cardName: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    cardRest: {
        fontSize: 25,
        color: "black",
        fontWeight: "500"
    },
    cardstars: {
        width: 60,
        height: 30,
        borderRadius: 10,
        backgroundColor: "green",
        paddingHorizontal: 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
    },
    cardstar: {
        fontSize: 18,
        fontWeight: "500",
        color: "white",
    },
    cardArea: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 3,
    },
    cardareaName: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
    },
    cardCost: {
        color: "grey",
        letterSpacing: 2,
        fontSize: 15,
        marginLeft: 10
    },
    cardTimes: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 5,
        marginBottom: 5,
    },
    cardTimeOrder:{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent:"space-between",
        marginBottom: 2,
    },
    cardTime: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
    },
    cardDistance: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
        marginLeft: 10
    },
    cardOffer: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 5,
    },
    cardOffer1: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
        color: "blue"
    },
    cardOffer2: {
        color: "grey",
        letterSpacing: 1.5,
        fontSize: 15,
        color: "blue",
        marginLeft: 10
    },
})