import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Gold from './Gold'
import Explore from './Explore'
import LimeLight from './LimeLight'
import Restaurants from '../Home/Restaurants'

const Dining = () => {
  return (
    <View>
      <ScrollView>
        <Gold/>
        <Explore />
        <LimeLight/>
        <Restaurants/>
      </ScrollView>
    </View>
  )
}

export default Dining

const styles = StyleSheet.create({})