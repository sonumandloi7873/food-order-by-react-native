import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import NavBar from '../../NavBar/NavBar'

const Gold = () => {
  return (
    <View style={{ height: 350}}>
        <NavBar />
        <Image
            style={styles.image}
            source={{
                uri: "https://b.zmtcdn.com/data/o2_assets/69963815381d03505031ca97cc396b871687856789.png"
            }} />
    </View>
  )
}

export default Gold

const styles = StyleSheet.create({
    image: {
        width: 410,
        height: 350,
        position: "absolute",
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        resizeMode: "stretch"
    },
})