import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const Explore = () => {
    return (
        <View>
            <View style={styles.expoler}>
                <View style={styles.titlteRow}></View>
                <Text style={styles.expolerTitle}>EXPLORE</Text>
                <View style={styles.titlteRow}></View>
            </View>
            <View style={styles.expolerOffer}>
                <View style={styles.expolerOfficon} >
                    <View style={styles.expolerOff}>
                        <Text style={styles.expoleradvance}>Walk-in offers</Text>
                        <Text style={styles.expolerper}>UP TO 40% OFF</Text>
                    </View>
                    <MaterialIcons style={styles.expolerOfferIcon} name="local-offer" size={30} />
                </View>
                <View style={styles.expolerOfficon}>
                    <View style={styles.expolerBook}>
                        <Text style={styles.expoleradvance}>Book in advance</Text>
                        <Text style={styles.expolerper}>UP TO 50% OFF</Text>
                    </View>
                    <MaterialCommunityIcons style={styles.expolerOfferIcon} name="calendar-month" size={30} />
                </View>
            </View>
        </View>
    )
}

export default Explore

const styles = StyleSheet.create({
    expoler: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    expolerTitle: {
        marginHorizontal: 15,
        letterSpacing: 5,
        color: "black",
        fontSize: 15
    },
    titlteRow: {
        width: 130,
        height: 1,
        backgroundColor: "#999999"
    },
    expolerOffer: {
        // borderWidth:2,
        width: 410,
        height: 100,
        marginTop: 20,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 5,

    },
    expolerOfficon: {
        // borderWidth: 1,
        width: 190,
        height: 80,
        borderRadius: 15,
        padding: 10,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        backgroundColor: "white",
        shadowColor:"black",
        shadowOffset: {width:1,height:1},
        shadowOffset:0.2,
        elevation: 5
    },
    expoleradvance: {
        fontSize: 18,
        fontWeight: "bold",
        color: "black"
    },
    expolerper: {
        fontSize: 13,
        fontWeight: "bold",
        color: "brown",
        borderWidth: 1,
        borderColor:'grey',
        paddingHorizontal: 5,
        borderRadius: 5,
        backgroundColor:"rgba(250, 50, 0, 0.2)",

    },
    expolerOfferIcon: {
        color: "purple",
    },
})