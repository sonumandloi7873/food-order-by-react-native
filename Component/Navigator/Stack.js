import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Tab from './Tab'
import useApiCall from '../Redux/CustomHook'
import LoginData from '../SingUp/LoginData'


const Stack = createNativeStackNavigator();

const StackS = () => {
  useApiCall()

  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="tab" component={Tab}
          options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default StackS

const styles = StyleSheet.create({})