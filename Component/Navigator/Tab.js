import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Home from '../Home/Home/Home'
import Dining from '../Home/Daining/Dining'
import Money from '../Home/Money/Money'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const Tab = () => {
    const Tab = createBottomTabNavigator()

    return (
            <Tab.Navigator screenOptions={{ tabBarStyle: { height: 55 , padding:5} }}>
                <Tab.Screen name="Delivery" component={Home}
                    options={{
                        // title: "",
                        tabBarLabel: ()=><Text style={{fontSize:18,color:"black"}} >Delivery</Text>,
                        tabBarIcon: () =><MaterialIcons name="delivery-dining" size={30} color="red" />,
                        headerShown:false

                    }} />
                <Tab.Screen name="Dining" component={Dining}
                    options={{
                        // title: "",
                        tabBarLabel: ()=><Text style={{fontSize:18,color:"black"}} >Dining</Text>,
                        tabBarIcon: () => <MaterialIcons name="dining" size={30} color="red" />,
                        headerShown:false

                    }} />
                <Tab.Screen name="Money" component={Money}
                    options={{
                        // title: "",
                        tabBarLabel: ()=><Text style={{fontSize:18,color:"black"}} >Money</Text>,
                        tabBarIcon: () => <MaterialIcons name="attach-money" size={30} color="red" />,
                        headerShown:false
                    }} />
  
            </Tab.Navigator>

    )
}

export default Tab

const styles = StyleSheet.create({})