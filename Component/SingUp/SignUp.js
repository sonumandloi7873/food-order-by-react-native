import { Button, StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getADD } from '../Redux/Slice'
import useApiCall from '../Redux/CustomHook'
// import AsyncStorage from '@react-native-async-storage/async-storage'

const SignUp = () => {

    // useSignUp()
    useApiCall()

    const dispatch = useDispatch()

    const [userName,setUserName]=useState("")
    const [userGmail,setUserGmail]=useState("")
    const [userPassword,setUserPassword]=useState("")

    function loginData(){
        return(
             setUserName(""),
            setUserGmail(""),
            setUserPassword(""),
            setData()
        )
    }

    const setData = async() =>{
        await AsyncStorage.setItem("name",userName);
        await AsyncStorage.setItem("email",userGmail);
        await AsyncStorage.setItem("password",userPassword);
        const name = await AsyncStorage.getItem("name")
        const email = await AsyncStorage.getItem("email")
        const password = await AsyncStorage.getItem("password")
        // console.log("name",name)
        dispatch(getADD({name,email,password}))
    }

  return (
    <View style={styles.form} >
        <View style={styles.formCard}>
            <TextInput placeholder ="userNme" value={userName} onChangeText={(text)=>setUserName(text)} style={styles.input} />
            <TextInput placeholder ="Gmail" value={userGmail} onChangeText={(text)=>setUserGmail(text)} style={styles.input} />
            <TextInput placeholder ="Password" secureTextEntry={true} value={userPassword} onChangeText={(text)=>setUserPassword(text)} style={styles.input} />
            <Button title='SignUP' onPress={loginData} /> 
        </View>
    </View>
  )
}

export default SignUp

const styles = StyleSheet.create({
    form:{
        flex:1,
        justifyContent:"center",
        alignItems:"center"

    },
    formCard:{
        // flex:1,
        justifyContent:"center",
        alignItems:"center",
        width:330,
        height:230,
        borderWidth:1,
        borderRadius:10,
        backgroundColor:"#e0a300",
        // shadowColor:"black",
        // shadowOffset: {width:1,height:1},
        // shadowOffset:0.2,
        // elevation: 5


    },
    input:{
        // borderWidth:2,
        marginBottom:5,
        borderRadius:10,
        height:40,
        width:250,
        padding:8,
        paddingHorizontal:15,
        backgroundColor:"white"
    }
})