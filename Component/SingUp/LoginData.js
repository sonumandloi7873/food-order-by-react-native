import { Button, Pressable, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import AsyncStorage from '@react-native-async-storage/async-storage'

const LoginData = ({ setUserLogin }) => {

    const userData =()=>setUserLogin(false)

    const  clearLocal = async()=>await AsyncStorage.clear()

    return (
        <View style={styles.form}>

            <View style={styles.formData}>
                <View style={styles.formDatacard}>
                    <Text style={styles.formDataIconText}>S</Text>
                    <View style={styles.formDataName}>
                        <Text style={styles.formDataNameText}>Sonu</Text>
                        <Text style={styles.formDataView}>view activity</Text>
                    </View>
                    <Pressable onPress={userData} style={{ position: "absolute", right: 2, top: 5 }}>
                        <AntDesign name="back" size={20} color="black"
                            style={{ borderRadius: 50, backgroundColor: "red", height: 30, padding: 5 }} />
                    </Pressable>

                </View>
                <View style={styles.formDataJoin}>
                    <Text style={styles.formDataGold}>Join Zomato Gold</Text>
                    <Text style={styles.formDataGold}>a</Text>
                </View>
            </View>

            <Button title='LogOut' onPress={clearLocal} />

        </View>
    )
}

export default LoginData

const styles = StyleSheet.create({
    form: {
        flex: 1,
        width: 200,
        height: 585,
        position: "absolute",
        top: 20,
        right: 0,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 10,
        zIndex: 100,
        justifyContent: "space-between"

    },
    formData: {
        // flex: 1,
        borderWidth: 1,
        borderRadius: 20

    },
    formDatacard: {
        // flex: 1,
        flexDirection: "row",
        height: 75,
        padding: 10,

    },
    formDataIconText: {
        backgroundColor: "lightblue",
        textAlign: "center",
        paddingTop: 5,
        borderRadius: 50,
        fontSize: 30,
        width: 50,
        height: 50,
        marginRight: 20

    },
    formDataName: {
        justifyContent: "space-between"

    },
    formDataNameText: {
        fontSize: 20,
        fontWeight: "bold"
    },
    formDataView: {
        color: "red",
    },
    formDataJoin: {

        // flex:1
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: 40,
        paddingHorizontal: 10,
        backgroundColor: "black",
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20
    },
    formDataGold: {
        color: "white",
        fontWeight: "bold"

    },
})