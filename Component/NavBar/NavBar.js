import { Pressable, SafeAreaView, StyleSheet, Text, TextInput, TouchableHighlight, TouchableOpacity, View, useColorScheme } from 'react-native'
import React, { useState } from 'react';
import SearchIcon from 'react-native-vector-icons/Feather';
import LocationIcon from 'react-native-vector-icons/Ionicons';
import LanguageIcon from 'react-native-vector-icons/FontAwesome';
import { useSelector } from 'react-redux';
import LoginData from '../SingUp/LoginData';

const NavBar = () => {
    console.log("tur", userLogin)

    // const data = useSelector((state) => state?.cartData?.LoginUser)
    // console.log("LoginUser",data.name[0])
    const data = {
        name: "Sonu"
    }
    const [userLogin,setUserLogin] = useState(false)

    const userData = () => setUserLogin(true)

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container_head}>
                <View style={styles.container_head_location}>
                    <LocationIcon name="location-sharp" size={30} color="red" />
                    <Text style={styles.container_head_location_text}>Indore</Text>
                    <LocationIcon name="chevron-down-sharp" size={20} color="red" />
                </View>
                <View style={styles.container_head_side}>
                    <View style={styles.container_head_languege}>
                        <LanguageIcon name="language" size={30} color="black" />
                    </View>
                    {
                        userLogin ? <LoginData setUserLogin={setUserLogin} /> :
                                <TouchableOpacity onPress={userData} style={styles.container_head_admin}>
                                    <Text style={styles.container_head_admin_text}  >{data.name[0]}</Text>
                                </TouchableOpacity>
                    }

                </View>
            </View>
            <View style={styles.container_search}>
                <SearchIcon name="search" size={30} color="red" />
                <TextInput placeholder='search' style={styles.container_search_input} />
                <Text style={{ fontSize: 20, padding: 2 }}>|</Text>
                <SearchIcon name="mic" size={25} color="red" style={{ padding: 4 }} />
            </View>
        </SafeAreaView>
    )
}

export default NavBar

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "column",
        backgroundColor: 'transparent',
        padding: 10,
        zIndex: 1,
    },
    container_head: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: 20

    },
    container_head_location: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    container_head_location_text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "red"
    },
    container_head_side: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 5,
        width: 100
    },
    container_head_languege: {
        borderWidth: 1,
        borderColor: "#A9A9A9",
        padding: 5,
        borderRadius: 5,
        backgroundColor: "#FFFAFA",

    },
    container_head_admin: {
        backgroundColor: "#ADD8E6",
        borderRadius: 20,
        // borderWidth:1,
        width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: 43,

    },
    container_head_admin_text: {
        color: "blue",
        fontSize: 30,

    },
    container_search: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        // borderWidth:1,
        borderColor: "#A9A9A9",
        borderRadius: 10,
        padding: 5,
        height: 50,
        backgroundColor: "white"
    },
    container_search_input: {
        width: 300,
    }


})